function [] = drawPZ(scanRes)
%drawPZ Draws the 2pz orbital
%%
a=(5e-11)/1e-10; % bohr radius in Angstroms, with reduced mass (PLACEHOLDER)
xRange=linspace(-2*a,2*a,scanRes); yRange=xRange; zRange=xRange;	%range over which wavefunction calculated
[xWaveMesh,yWaveMesh,zWaveMesh]=meshgrid(xRange,yRange,zRange);	% mesh of positions for calculation
rWaveMesh=sqrt(xWaveMesh.^2+yWaveMesh.^2+zWaveMesh.^2);	%radial distance

wavePZ=waveFunction(rWaveMesh,zWaveMesh);	%calc wavefunction density for each position in mesh
probDensityPZ=wavePZ.^2;

figure('Name','Hydrogen p_z orbital');clf
isosurface(xWaveMesh,yWaveMesh,zWaveMesh,probDensityPZ,1,wavePZ)
xlabel('x (Angstroms)'); ylabel('y (Angstroms)'); zlabel('z (Angstroms)')
axis equal
end