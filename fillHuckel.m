function [ huckel ] = fillHuckel( pos, plotFlag )
%fillHuckel Fills the huckel matrix (and plots the molecule with bonds)
%% Huckel pre-set up
nAtoms=size(pos,1);							% number of atoms
a=0;	% Coulomb integral, set to 0 as only shifts energy levels up/down...
% represents interaction of electron with its own nucleus
b=-1;	% bond/resonance integral, represents interaction of electron with...
% two nuclei simultaneously
huckel=a*eye(nAtoms);
%% plot atoms
	
if nargin==2%number of input arguments
	plotFlag=true;
else
	plotFlag=false;
end
if plotFlag==true
	figure('Name','C60, buckminsterfullerene'); % plot position of each carbon atom
	plot3(pos(:,1),pos(:,2),pos(:,3),'ko','MarkerSize',10)
	axis equal
% 	[r,theta,phi]=cart2sphPhys(pos(:,1),pos(:,2),pos(:,3));
% 	[x,y,z]=sph2cartPhys(r+0.4,theta,phi);	% move each position out a little for numbering
% 	text(x,y,z,{1:nAtoms},'FontSize',16);	% number each atom
% 	xlabel('x (Angstroms)');ylabel('y (Angstroms)');zlabel('z (Angstroms)')
end

%% find (and plot bonds)
neigh=knnsearch(pos,pos,'K',4);		% find 4 nearest neighbours
for atom=1:nAtoms				% loop through each atom
	xAtom=pos(atom,1); yAtom=pos(atom,2); zAtom=pos(atom,3);	% position of a carbon atom
	for k=2:4		% loop through each of 3 nearest neighbours of that atom
		neighIndex=neigh(atom,k);
		huckel(atom,neighIndex)=b;
		if plotFlag==true
			xNeigh=pos(neighIndex,1);	yNeigh=pos(neighIndex,2);	zNeigh=pos(neighIndex,3);	% position of each nearest neighbour
			h=line([xAtom xNeigh],[yAtom yNeigh],[zAtom zNeigh]);	% draw line from atom to nearest neighbours
			set(h,'LineSmoothing','on')		% make pretty
			if k==4							% longest so double bond
				set(h, 'LineWidth',2)		% make double bonds appear wider
			else							% single bonds
				set(h, 'LineWidth',1)
			end
		end
	end
end
if plotFlag==true
	[r,theta,phi]=cart2sphPhys(pos(:,1),pos(:,2),pos(:,3));
	[x,y,z]=sph2cartPhys(r+0.4,theta,phi);	% move each position out a little for numbering
	text(x,y,z,{1:nAtoms},'FontSize',24);	% number each atom
	xlabel('x (Angstroms)');ylabel('y (Angstroms)');zlabel('z (Angstroms)')
end
end

