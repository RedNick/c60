function [ wave ] = waveFunction( r,z )
%waveFunction Calculates a p_z atomic orbital
%%
	n=2;
	Z=6;	%atomic number
	a=(5e-11)/1e-10; % bohr radius in Angstroms, with reduced mass (PLACEHOLDER)
	wave=(Z/a)^(5/2)*sqrt(3/(24*pi))/n*z.*exp(-Z*r/(n*a));
end

