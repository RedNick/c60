function [ cellAfter ] = negCell( cellBefore )
%negCell Takes a cell and changes sign of every element of every matrix 
% only tested on square matrices
%%
cellSize=size(cellBefore);
matrixVersion=-cell2mat(cellBefore);
matrixSize=size(matrixVersion);
nMatrices=matrixSize./cellSize;
cellAfter=mat2cell(matrixVersion,nMatrices(1)*ones(1,cellSize(1)),nMatrices(2)*ones(1,cellSize(2)));
end

