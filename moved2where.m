function [ moveStore ] = moved2where( pos,atom,T1u )
%move2where Calculates where a particular atom moves to using T1u basis
moveStore=zeros(120,1);
posAtom=pos(atom,:);
for i=1:120
	transformation=T1u{i};
	posAtomMoved=transformation*posAtom';
	movedToAtom=knnsearch(pos,posAtomMoved','K',1);
	moveStore(i)=movedToAtom;
end
end

