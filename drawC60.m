function [ output_args ] = drawC60( eigenVectors,pos,energyLevels,scanRes )
%drawC60 [] = drawC60( eigenVectors,pos,energyLevels,scanRes )
%%
xRange=linspace(-6,6,scanRes);yRange=xRange;zRange=xRange;
[xMesh,yMesh,zMesh]=meshgrid(xRange,yRange,zRange); % mesh of positions for calculation
[probDensityTotal,waveTotal] = probDensity(xMesh,yMesh,zMesh,eigenVectors,pos,energyLevels);

%% draw (made separate to save calclulation time)
figure('Name','Constant wavefunction isosurface of C60');clf
isosurface(xMesh,yMesh,zMesh,probDensityTotal,1e-8,waveTotal) %3d plot of surface with constant probDensity
xlabel('x (Angstroms)');ylabel('y (Angstroms)');zlabel('z (Angstroms)')
axis equal
end