function [ x,y,z ] = sph2cartPhys( r,theta,phi )
%sph2cartPhys like sph2cart but using Physics convention
%   r=radial dist, theta=angle from z down towards x, phi=angle from x
%   towards y
%%
rSinTheta=r.*sin(theta);	%save a little computation time
x = rSinTheta.*cos(phi);
y = rSinTheta.*sin(phi);
z = r.*cos(theta);
end

