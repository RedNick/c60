function [ heightResults ] = constCurrent( eigenVectors,pos,energyLevels,current,range,scanRes )
%constCurrent Performs constant current STM
%%
xyPos=linspace(-range,range,scanRes);
[xMesh,yMesh]=meshgrid(xyPos,xyPos);
heightResults=zeros(scanRes);

startHeight=max(pos(:,3))+1; %height of highest atom + a little
height=startHeight;

for xN=1:scanRes
	x=xyPos(xN);	% x position at which to scan
	for yN=1:scanRes
		y=xyPos(yN);	% y position at which to scan
		[height,flag] = findHeight(height,current,x,y,eigenVectors,pos,energyLevels);
		if flag == true		% 
			heightResults(xN,yN)=height;
		end
	end
end

% %% plot scan positions
% figure('Name','Constant current STM of C60, height plot')
% plot3(xMesh,yMesh,heightResults','x')
% xlabel('x (Angstroms)');ylabel('y (Angstroms)');zlabel('scan height (Angstroms)');
% axis equal

%% plot contour
figure('Name','Constant current STM of C60, contour plot')
colormap('gray')
contourf(xMesh,yMesh,heightResults',200,'LineStyle','none')

xlabel('x (Angstroms)');ylabel('y (Angstroms)');
axis equal

end

