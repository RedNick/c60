function [ height, flag ] = findHeight(startHeight, current, x, y, eigenVectors,pos,energyLevels)
%findHeight Finds the height at which a certain current flows (prob density
%for now)
%%
probDensity2 = @(h) probDensity(x,y,h,eigenVectors,pos,energyLevels)-current;
options=optimset('TolX',1e-6);
[heightTemp,fval,exitflag]=fzero(probDensity2,startHeight,options);
if exitflag==1
		height=heightTemp;
		flag=true;
		if heightTemp < 0
			flag = true;
		end
	else
		height=startHeight;
		flag=false;
end
end

