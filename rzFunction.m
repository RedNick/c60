function [ r,z ] = rzFunction( pos,atom,xMesh,yMesh,zMesh )
%rzFunction Finds r and z relative to an atom at every point on a mesh
%%
xAtom=pos(atom,1);							% position of a carbon atom
yAtom=pos(atom,2);
zAtom=pos(atom,3);

xLocalMesh=xMesh-xAtom;
yLocalMesh=yMesh-yAtom;
zLocalMesh=zMesh-zAtom;
r=sqrt(xLocalMesh.^2+yLocalMesh.^2+zLocalMesh.^2);

modn=sqrt(xAtom^2+yAtom^2+zAtom^2);
z=(xAtom*xLocalMesh +yAtom*yLocalMesh +zAtom*zLocalMesh)/modn;
end

