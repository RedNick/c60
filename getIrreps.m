function [ irreps,character,T1u ] = getIrreps()
%getIrreps generates the irreps for the full icosahedral group
%% Load representation matrices provided by Dr Dunn
run('Repesentation_matrices.m');

%% Generate the irreducable representations
Ag=cat(2,AM,AM);				%singlet (even (gerade))
Au=cat(2,AM,negCell(AM));		%singlet (odd (ungerade))
T1g=cat(2,T1M,T1M);				%triplet (even (gerade))
T1u=cat(2,T1M,negCell(T1M));	%triplet (odd (ungerade))
T2g=cat(2,T2M,T2M);				%triplet (even (gerade))
T2u=cat(2,T2M,negCell(T2M));	%triplet (odd (ungerade))
Gg=cat(2,GM,GM);				%quadruplet (even (gerade))
Gu=cat(2,GM,negCell(GM));		%quadruplet (odd (ungerade))
Hg=cat(2,HM,HM);				%quadruplet (even (gerade))
Hu=cat(2,HM,negCell(HM));		%quadruplet (odd (ungerade))

% irreps=vertcat(Ag,T1g,T2g,Gg,Hg,Au,T1u,T2u,Gu,Hu);
irreps=vertcat(Ag,Au,T1g,T1u,T2g,T2u,Gg,Gu,Hg,Hu);
%%
character=zeros(10,120);
% r=(1+sqrt(5))/2;	%golden ratio
for i = 1:10
	for j=1:120
		character(i,j)=trace(irreps{i,j});
	end
end
end

