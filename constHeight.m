function [probDensityTotal] = constHeight(eigenVectors,pos,energyLevels,up,range,scanRes)
%constHeight performs constant height STm
%%
z=max(pos(:,3))+up;	%height at which to do STM
xRange=linspace(-range,range,scanRes);yRange=xRange;	
[xMesh,yMesh]=meshgrid(xRange,yRange);		% mesh of positions for calculation
[probDensityTotal,waveTotal] = probDensity(xMesh,yMesh,z,eigenVectors,pos,energyLevels);
%% plot contours
figure('Name','Constant height STM of C60 Contours');
colormap('gray')
contourf(xMesh,yMesh,probDensityTotal,500,'LineStyle','none')
xlabel('x (Angstroms)');ylabel('y (Angstroms)');
axis equal
end

