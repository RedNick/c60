function [ r,theta,phi ] = cart2sphPhys( x,y,z )
%cart2sphPhys like cart2sph but using Physics convention
%   r=radial dist, theta=angle from z down towards x, phi=angle from x
%   towards y
%%
	r = sqrt(x.^2+y.^2+z.^2);
	theta = acos(z./r);
	phi = atan2(y,x);
end

