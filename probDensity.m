function [probDensityTotal,waveTotal] = probDensity(x,y,z,eigenVectors,pos,energyLevels)
%probDensity Calculates probability density for point (or mesh)
%   (x,y,z)= co-ordinates of meshes or a point (can be a combination, i.e 
%	xMesh,yMesh, and a single z [if any are a mesh, x MUST be a mesh]).
%	eigenVectors = eigenvectors of molecule
%	pos=positions of atoms, energyLevels=
%%
waveTotal=zeros(size(x));
probDensityTotal=zeros(size(x));

for energyLevel=energyLevels;
	waveLevel=zeros(size(x));
	probDensityLevel=zeros(size(x));
	for atom=1:size(pos,1)							% loop through each atom
		[rLocalMesh,zNew]=rzFunction( pos,atom,x,y,z ); %convert to axis centred and aligned on the atom
		huckelCoeff=eigenVectors(atom,energyLevel);
		waveAtom=huckelCoeff*waveFunction(rLocalMesh,zNew);
		waveLevel=waveLevel+waveAtom;
		probDensityLevel=waveLevel.^2;
	end
	waveTotal=waveTotal+waveLevel;
	probDensityTotal=probDensityTotal+probDensityLevel;
end
end

