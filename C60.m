%%	C60 Scanning Tunneling Microscopy
%% clear memory
clear all; close all;
%% make figures a bit better for LaTeX
set(0,'defaultLineMarkerSize',12)
set(0,'defaultAxesFontSize', 24)
set(0,'defaultTextFontSize', 18)

%% Load atom positions and count atoms
pos = C60Import();
nAtoms=size(pos,1);							% number of atoms

%% find (and plot bonds) and fill up Huckel matrix
plotFlag=true;
huckelUnNormalized=fillHuckel( pos, plotFlag );
normalization=sqrt(sum(huckelUnNormalized.^2));
huckel=bsxfun(@rdivide,huckelUnNormalized,normalization);

%% Calculate energy eigenvalues
tic
[eigenVectors,eigenValuesTemp]=eig(huckel); %calc eigenvectors and eigenvalues of Huckel matrix
toc
sum(eigenVectors.^2,2) % can use to check normalization
eigenValues=eigenValuesTemp*ones(nAtoms,1); % reformats to familiar n-by-1

%% Save eigenVectors, eigenValues, Huckel matrix, pos
save('huckelSaved','eigenVectors','eigenValues','huckel','pos')

%% Plot energy eigenvalues
figure('Name','Allowed energy levels for MOs ( \beta)');
plot(eigenValues,'x','MarkerSize',10)
ylabel('Allowed energy levels for MOs ( \beta)')
xlim([-2 nAtoms+2])		% make graph a little better

%% The p_z Wavefunction
scanRes=100;
% drawPZ(scanRes);

%%
% Zpoint=mean(pos([18,14,17,19,34,24],:))	%hexagon up
% Zpoint=mean(pos([9,11,14,15,18],:)) % pentagon up
Zpoint = pos(1,:)		% an atom up 
[Q,R]=qr(Zpoint')	%generate orthonormal basis

zC2hat=-Q(1,:)
xC2hat=Q(3,:)
yC2hat=Q(2,:)

posRotatedX=xC2hat(1)*pos(:,1)+xC2hat(2)*pos(:,2)+xC2hat(3)*pos(:,3);
posRotatedY=yC2hat(1)*pos(:,1)+yC2hat(2)*pos(:,2)+yC2hat(3)*pos(:,3);
posRotatedZ=zC2hat(1)*pos(:,1)+zC2hat(2)*pos(:,2)+zC2hat(3)*pos(:,3);
pos=[posRotatedX,posRotatedY,posRotatedZ];

%% p_z orbitals on C60
energyLevels=(nAtoms/2+1):(nAtoms/2+3);		%LUMO
% energyLevels=(nAtoms/2-4):(nAtoms/2);		%HOMO
% energyLevels=1;		%lowest
scanRes=10;	% resolution of mesh
% drawC60( eigenVectors,pos,energyLevels,scanRes );

%% constant height STM
range=3;
scanRes=500;
up=0.3;	%height above highest atom at which to scan
probDensityTotal = constHeight(eigenVectors,pos,energyLevels,up,range,scanRes);
drawnow
%% constant current STM
up=3;
current=1e-7;	%not really the current, yet...
scanRes=40;		%scan resolution
heightResults=constCurrent(eigenVectors,pos,energyLevels,current,range,scanRes);


%% Group theory
[ irreps,character,T1u ] = getIrreps();
atom=1;
moveStore = moved2where( pos,atom,T1u );	%checks where atom 1 moves to under each transformation
rep=0;
symWaveVect=zeros(60,120);	% to put the symmetry adjusted wavevectors in
for irrepN=1:size(irreps,1)	% loop through each irreducible representation
	projOp=zeros(size(irreps{irrepN,1}));
	for row=1:length(projOp)	
		for col=1:length(projOp) % loop through each position in the irrep
			rep=rep+1;	% each matrix position in each irrep is a new proj Op
			for r=1:120	% loop through each symmetry operation
				irrep=irreps{irrepN,r};
				movedWhere=moveStore(mod(r-1,60)+1);
				symWaveVect(movedWhere,rep)=symWaveVect(movedWhere,rep)+irrep(row,col);
			end
		end
	end
end

%% Checking for dupes manually
margin=1e-5;
normalization=sqrt(sum(symWaveVect.^2));	%normalise each of the wave vectors
waveVecthat=bsxfun(@rdivide,symWaveVect,normalization);
U=waveVecthat(:,1);
for i=2:size(waveVecthat,2)	% loop through each wave vector
	newVector=waveVecthat(:,i);
	same=false;
	for n=1:size(U,2)	% loop through each already used vector
		compareVector=U(:,n);
		differenceA=newVector-compareVector;
		overallDiffA=sum(differenceA.^2);	%work out difference between vectors
		differenceB=newVector+compareVector; %don't want ones that are just -1* another
		overallDiffB=sum(differenceB.^2);
		if (overallDiffA < margin) || (overallDiffB < margin)
			same=true;	% discard any that are the same as any already used
			break
		elseif isnan(newVector)
			same=true;	% discard any that are NaN
			break
		end
	end
	if same==true
		%discard this one
	else
		U=horzcat(U,newVector);	% append the new vector on
	end
end

I=U'*U;	% check that U is indeed unitary
newHuckel=U'*huckel*U; % calculate our new transformed huckel
tic
[newEigenVectors,D]=eig(newHuckel); %eigenvals and eigenvectors for new huckel
toc
newEigenValues=real(D*ones(nAtoms,1));
newEigenVectors=real(newEigenVectors);  %get rid of tiny complex bits (from errors in calcs)
roundedHuckel=round(10000*newHuckel)/10000;
tic
eig(roundedHuckel);			% this is much faster than original huckel
toc
digits(3)
latex(vpa(sym(roundedHuckel,'d')));

%% Plot energy eigenvalues for symmetry adjusted
figure('Name','Allowed energy levels for MOs ( \beta)');
[sortedEigenValues, sortIndex]=sort(newEigenValues); % sort eigenvalues into ascending order
plot(sortedEigenValues,'x','MarkerSize',10)
ylabel('Allowed energy levels for MOs ( \beta)')
xlim([-2 nAtoms+2])		% make graph a little better

%% Sort eigenvectors into right order (i.e. same order as energy levels)
sortedEigenVectors=zeros(60,0);
for i=1:60
	index=sortIndex(i);
	temp=newEigenVectors(:,index);
	sortedEigenVectors=horzcat(sortedEigenVectors,temp);	
end

%% from symmetry adjusted wavefunctions back to plotable ones
plotableEig=zeros(60,0);
for i=1:60
	temp=sortedEigenVectors(:,i);
	plotableEigTemp=U*temp;
	plotableEig=horzcat(plotableEig,plotableEigTemp);
end

%% p_z orbitals on C60 for symmetry adjusted
energyLevels=(nAtoms/2+1):(nAtoms/2+3);		%LUMO
% energyLevels=(nAtoms/2-4):(nAtoms/2);		%HOMO
% energyLevels=1;		%lowest
scanRes=120;	% resolution of mesh
drawC60( plotableEig,pos,energyLevels,scanRes );

%% constant height STM for symmetry adjusted
range=4;
scanRes=500;
up=0.3;	%height above highest atom at which to scan
probDensityTotal = constHeight(plotableEig,pos,energyLevels,up,range,scanRes);

%% constant current STM for symmetry adjusted
up=3;
current=1e-15;	%not really the current, is proportional to the current
scanRes=20;		%scan resolution
heightResults=constCurrent(plotableEig,pos,energyLevels,current,range,scanRes);


