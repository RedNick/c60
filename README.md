C60 - Nicolas Redfern
=====================
LaTeX
----------
- Folder containing LaTeX code for report

C60.m
--------------
- Reads in C60Positions.txt creates 3D image of molecule with bonds drawn on
- Finds each atom's nearest neighbour and create Huckel matrix
- Solves secular determinant of Huckel matrix, to find energy eigenvalues and plots
- Plots p_z wavefunction
- Plots p_z orbitals on C60 for chosen energy levels
- Simulates constant height STM on C60
- Simulates constant current STM on C60

C60Jmol
-------
- Model of C60 created using Jmol

C60Positions.txt
----------------
- Positions of the carbon atoms found using Jmol, as a comma separated list

cart2sphPhys.m
--------------
- Function like MATLAB's built in cart2sph but using Physics convention

findHeight.m
------------
- Function, finds the height at which a certain current flows

probDensity.m
-------------
- Calculates probability density for point (or mesh)

README.md
---------
- This file

rzFunction.m
------------
- Finds r (radial distance) and z (distance in z-direction) relative to an atom at every point on a mesh

sph2cartPhys.m
--------------
- Like sph2cart but using Physics convention

waveFunction.m
--------------
- Calculates a p_z atomic orbital




